#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

size_t testsPassed = 0;

static struct block_header *block_get_header(void *contents) {
  return (struct block_header*) (((uint8_t*) contents) - offsetof(struct block_header, contents));
}

void test(bool test) {
  if (!test) {
    printf("FAILED\n\n");
  } else {
    testsPassed++;
    printf("PASSED\n\n");
  }
}

void printTest(int test_num, char* test_name) {
  printf("Test №%d: %s\n", test_num, test_name);
}

void* startTest() {
  void* addr = heap_init(REGION_MIN_SIZE);
  return addr;
}

void endTest() {
  heap_term();
}

bool mallocTest() {
  printTest(1, "mallocTest");
  void* heap = startTest();
  if (!heap) {
    endTest();
    return false;
  }
  endTest();
  return true;
}

bool freeBlock() {
  printTest(2, "freeBlock");
  void* heap = startTest();
  if (!heap) {
    endTest();
    return false;
  }

  void *ptr1 = _malloc(1);
  struct block_header *block = block_get_header(ptr1);

  if (block->is_free) {
    endTest();
    return false;
  }

  _free(ptr1);
  if (!block->is_free) {
    endTest();
    return false;
  }

  void *ptr2 = _malloc(2);
  if (ptr1 != ptr2) {
    endTest();
    return false;
  }

  endTest();
  return true;
}

bool twoFreeBlock() {
  printTest(3, "twoFreeBlock");
  void* heap = startTest();
  if (!heap) {
    endTest();
    return false;
  }

  void *ptr1 = _malloc(10);
  void *ptr2 = _malloc(10);
  struct block_header *block1 = block_get_header(ptr1);
  struct block_header *block2 = block_get_header(ptr2);

  if (block1->is_free || block2->is_free) {
    endTest();
    return false;
  }

  _free(ptr1);
  if (!block1->is_free || block2->is_free) {
    endTest();
    return false;
  }

  _free(ptr2);
  if (!block1->is_free || !block2->is_free) {
    endTest();
    return false;
  }

  endTest();
  return true;
}

bool extendMem() {
  printTest(4, "extendMem");
  void* heap = startTest();
  if (!heap) {
    endTest();
    return false;
  }

  void *ptr = _malloc(REGION_MIN_SIZE + 10);
  if (HEAP_START + offsetof(struct block_header, contents) != ptr) {
    endTest();
    return false;
  }
  
  endTest();
  return true;
}

bool extendMemDifferentPlace() {
  printTest(5, "extendMemDifferentPlace");
  void* heap = startTest();
  if (!heap) {
    endTest();
    return false;
  }

  void* data = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

  _malloc(REGION_MIN_SIZE + 2);
  if (!data) {
    endTest();
    return false;
  }
  
  endTest();
  return true;
}

int main() {
  test(mallocTest());
  test(freeBlock());
  test(twoFreeBlock());
  test(extendMem());
  test(extendMemDifferentPlace());
  printf("RESULT: %zu/5 tests passed\n", testsPassed);
  return 0;
}